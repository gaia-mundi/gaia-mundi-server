/**
 * filter une list de jeux de donnée sur la maille et l'echelle
 * @param {*} list la liste des jeux de données à filterer 
 * @param {*} maille 
 * @param {*} echelle 
 * @returns 
 */
function filterOnMailleAndEchelle(list, maille, echelle ){
    let filteredList = [];

    list.map((dataSet => {
        if(dataSet.echelle === echelle && dataSet.maille === maille){
            filteredList.push(dataSet);
        }
    }))
    return filteredList;
}

module.exports = {filterOnMailleAndEchelle}