const AdmZip = require("adm-zip");
const fs = require("fs");


function extractMapInfoFile(filepath, file, tempDirectory) {
  try {
    const zipToExtract = new AdmZip(filepath);
    // on extrait le fichier mapInfo.json qui contient les infos basiques de la carte dans un dossier temporaire
    zipToExtract.extractEntryTo(file, tempDirectory, false, true);
  } catch (e) {
    console.log("Something went wrong : " + e);
  }
}

function readZipArchive(mapBaseFolder, tempDirectory, jsonTarget) {
  let tabListing = [];
  // Lecture des fichiers dans le dossier des fonds de carte
  const listing = fs.readdirSync(mapBaseFolder);
  // On liste les fichier et on effectue des actions
  listing.forEach(function(file) {
    try {
      console.log(file);
      const zip = new AdmZip(mapBaseFolder + file);
      for (const zipEntry of zip.getEntries()) {
        if (zipEntry.name === jsonTarget) {
          // extraction du fichier mapInfo.json pour récupérer les données
          extractMapInfoFile(mapBaseFolder + file, zipEntry.name, tempDirectory);
          // lecture du fichier
          const object = fs.readFileSync(tempDirectory + zipEntry.name);
          // écriture des données dans un tableau
          console.log(object);
          tabListing.push(JSON.parse(object));
          // suppression du fichier mapInfo.json traité
          fs.unlinkSync(tempDirectory + zipEntry.name);
        }
      }
    } catch (e) {
      console.log("Something went wrong : " + e);
    }
  });
  return tabListing;
}

function getZipPath(mapBaseFolder, index) {
  let files = "";
  
  const listing = fs.readdirSync(mapBaseFolder);
  // On liste les fichier et on effectue des actions
  if(isNaN(index)){
    listing.forEach(function(file) {
        if(file == index+".zip") {
          files = mapBaseFolder + file;
        }
    });
  } else {
    listing.forEach(function(file, i) {
        if(i == index) {
          files = mapBaseFolder + file;
        }
    });
  }
  return files;
}

module.exports = { readZipArchive, getZipPath };