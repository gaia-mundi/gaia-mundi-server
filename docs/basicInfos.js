module.exports = {
    openapi: "3.0.3", // present supported openapi version
    info: {
        title: "Refonte SuiteCairo", // short title.
        description: "documentation technique de la refonte de la SuiteCairo", //  desc.
        version: "1.0.0", // version number
        contact: {
        name: "CGI / VDL", // your name
        email: "", // your email
        url: "", // your website
        },
        license: {
        name: "GNU LGPLv3",
        url: "https://www.gnu.org/licenses/lgpl-3.0.en.html",
        },
    },
};