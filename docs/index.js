const basicInfo = require('./basicInfos');
const tags = require('./tags');

module.exports = {
    ...basicInfo,
    ...tags,
};