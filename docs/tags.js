module.exports = {
    tags: [
      {
        name: "Dépôt d'une carte",
      },
      {
        name: "Modification des informations d'une carte",
      },
      {
        name: "Afficher une carte",
      },
      {
        name: "Intégration de données dans une carte",
      },
      {
        name: "Création d'un menu de données",
      },
      {
        name: "Affichage des données sur la carte",
      },
      {
        name: "Export de la carte en PageCarto",
      },
      {
        name: "Transfert de données",
      },
      {
        name: "Transfert d'un menu de données",
      },
    ],
  };