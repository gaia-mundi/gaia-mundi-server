const archiveUtils = require('../services/readArchive');
const contourService = require('../services/contours')
const constants = require('./../services/constants.js')

exports.getContourList = (req, res, next) => {

    //récupérer les paramètres de filtrage 
    let echelle = req.query.echelle;
    let maille = req.query.maille;

    if (echelle && maille) {

        //récupérer la liste des contours depuis les archive
        let contourList = archiveUtils.readZipArchive(constants.contourBaseFolder, constants.tempDirectory, constants.contourJsonTargetFileName);

        //filter la liste des contours sur la maille et l'echelle
        let filteredList = contourService.filterOnMailleAndEchelle(contourList, maille, echelle)

        res.status(200).json(filteredList);

    } else {
        res.status(400).json({ message: "manque de query paramters echelle et maille" });
    }
}

/**
 * récuperation du fichier zip du contour
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
 exports.getContourZip = (req, res, next) => {
    let contourName = req.params.contourName;

    let url = constants.contourBaseFolder + contourName  + '.zip';
    
    res.download(url);
}
