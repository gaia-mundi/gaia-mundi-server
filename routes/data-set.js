const express = require('express');

const router = express.Router();
const  dataSetCtrl = require('../controllers/data-set')

router.get('/', dataSetCtrl.getDataSetList);
router.get('/:dataSetName', dataSetCtrl.getDataSetZip);

module.exports = router;