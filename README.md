# GaïaMundi – Serveur

La solution GaïaMundi est une solution client-serveur.

Le client est une application Electron JS qui donne accès à l’ensemble
des fonctionnalités de l’application et la partie serveur est un serveur
headless développé en NodeJS.

Cette solution ne fait pas intervenir de base de données l’ensemble des
éléments sont persistés sur fichier que ce soit coté serveur ou coté
client lourd (Electron JS).

Les communications entre client et serveur se font via des API REST au
format JSON.

Pour consulter la partie client, veuillez vous rendre au dépôt suivant : [https://gitlab.com/gaia-mundi/gaiamundi-client](https://gitlab.com/gaia-mundi/gaiamundi-client)

## Présentation

Le serveur est un serveur headless développé en NodeJS.

- L’ensemble de services exposés utilise la librairie
  [Express](https://expressjs.com/fr/).

- Le système de stockage est constitué d’un ensemble de fichiers

Le packaging est décrit de manière standard via le fichier
_package.json_

## Développement

### Pré-requis

- NodeJS v16 : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
- NPM (Node Package Manager)

### Installation

Il faut tout d'abord installer les dépendences du projet à travers la commande suivante :

```sh
npm install
```

### Démarrer le serveur locale

Pour démarrer le serveur sur votre machine, utilisez la commande :

```sh
npm run start
```

La commande permet de démarrer le serveur sur le port 3000 : http://localhost:3000/.

## Terminologies

- Couche : L'affichage des carte côté client se fait à travers la superposition d'une ou plusieurs couches. Chaque couche peut être de type `contours` (exp. périmétres des régions) ou `data` (les données géolocalisés).

- Dataset : Un jeu de données qui consiste essentiellement en un tableau de données. Un jeu de données est importé à l'aide d'un fichier tableur (CSV).

- Workspace : Espace de travail qui permet de définir une carte. Chaque espace de travail est relative à une thématique. Une espace de travail contient nécessairement des couches de contours/données.

- Model/Template : Un template constitue un modèle d'un espace de travail que l'on peut instancier et modifier. C'est un moyen rapide pour créer un espace de travail.

- Map : Le fond de carte constitue la premiére couche surlaquelle va se poser les autres couches, contours/données.

## Structure du code

Le fichier principal de l’application est _server.js_.

Ce fichier créer un serveur http écoutant sur le port défini par la
variable d’environnement PORT. Par défaut le port est 3000.

Au démarrage le serveur est configuré en utilisant la configuration
définie par le fichier _app.js_ :

```
const app = require('./app');

…

const server = http.createServer(app);
```

Le fonctionnement global est le suivant :

Les routes définies dans le répertoire _routes_ font appel à un
contrôleur définie dans le répertoire _controllers_ qui s’appuie sur les
fonctionnalités métiers du répertoire _services_ pour répondre aux
appels web.

## Définition des services exposés : fichier app.js

Le fichier _app.js_ utilise la librairie
[Express](https://expressjs.com/fr/) pour définir l’ensemble des
services exposés.

Ces services sont définis dans les fichiers du répertoire _routes_ :

- _contours.js_ : permet d’exposer les services relatifs aux couches
  ‘contours’. Ces services sont implémentés dans le fichier
  _controllers/contours.js_. Sont exposés les services :

  - \[GET\]/api/contours : récupère la liste des couches ‘contours’
    disponible,

  - \[GET\]/api/contours/:contourName : une couche ‘contours’ par
    son identifiant (contourName).

- _data-set.js_ : permet d’exposer les services relatifs aux couches
  ‘data’. Ces services sont implémentés dans le fichier
  _controllers/data-set.js_. Sont exposés les services :

  - \[GET\]/api/data-sets : récupère la liste des couches ‘data’
    disponible,

  - \[GET\]/api/ data-sets /:dataSetName : une couche ‘data’ par
    son identifiant (dataSetName).

- _map.js_ : permet d’exposer les services relatifs aux fond de
  cartes. Ces services sont implémentés dans le fichier
  _controllers/map.js_. Sont exposés les services :

  - \[GET\]/api/map : récupère la liste des fonds de carte
    disponible,

  - \[GET\]/api/map/:id : récupéré un fond de carte par son
    identifiant (id).

- _workspace.js_ : permet d’exposer les services relatifs aux espaces
  de travail. Ces services sont implémentés dans les fichiers
  _controllers/workspace.js_ et _middleware/workspace-upload.js_. Sont
  exposés les services :

  - \[GET\]/api/workspaces : récupère la liste espaces de travail
    disponible,

  - \[POST\]/api/workspaces : télé versement d’un espace de travail
    sur le serveur. Ce service utilise un middleware Express
    (intercepteur) pour gérer le stockage des flux entrants (voir
    §_Le répertoire middleware_)

  - \[GET\]/api/workspaces/models : récupération de la liste des
    templates pour créer un nouvel espace de travail.

  - \[GET\]/api/workspaces/models/:id : récupération template
    d’espace de travail par son identifiant (id).

## Le répertoire controllers

Ce répertoire contient la déclaration des services REST de l’application.

### Fichier contours.js

- La fonction getContourList permet de récupérer la liste des couche
  ‘contours’.  
  Ces couches contours sont stockées dans le répertoire
  _data/contours_. Chaque couche est définie selon le format précisé
  dans le paragraphe _Structure des couches contours_.  
  La récupération de la liste des informations se fait via la méthode
  readZipArchive du fichier _service/readArchive.js_.

- La fonction getContourZip permet de récupérer l’archive de la couche
  contour sélectionné

### Fichier data-set.js

- La fonction getDataSetList permet de récupérer la liste des jeux de
  données.  
  Ces jeux sont stockées dans le répertoire _data/data_sets_. Chaque
  jeux est défini selon le format précisé dans le paragraphe
  *Structure des jeux de donnée*s.  
  La récupération de la liste des informations se fait via la méthode
  readZipArchive du fichier _service/readArchive.js_.

- La fonction getDataSetZip permet de récupérer l’archive du jeu de
  données

### Fichier map-set.js

- La fonction getMapList permet de récupérer la liste des fond de
  carte.  
  Ces fons sont stockées dans le répertoire _data/Fond_de_cartes_.
  Chaque fond est défini selon le format précisé dans le paragraphe
  Structure des fonds de carte *Structure des jeux de donnée*s.  
  La récupération de la liste des informations se fait via la méthode
  readZipArchive du fichier _service/readArchive.js_.

La fonction getMap permet de récupérer l’archive du jeu de données

<span id="_Toc116638811" class="anchor"></span>

### Fichier workspace.js

Ce fichier est en charge de la gestion des espaces de travail, ainsi que
des templates.

- La fonction getWorkspaceList permet de récupérer la liste des
  espaces de travail. Ces espaces sont stockés dans le répertoire
  _data/Workspaces_. Chaque espace est définie selon le format précisé
  dans le paragraphe _Structure d’un fichier de définition d’un espace
  de travail_.

- La fonction getWorkspace permet de récupérer l’archive associé à
  l’identifiant.

- La fonction getWorkspacesModelsList permet de récupérer un modèle
  d’espace de travail. Ces templates ont la même structure que les
  espaces de travail mais ils n’ont pas de scénario défini. Ils sont
  stockés dans le répertoire _data/wsmodels_

- La fonction getWorkspaceModel permet de récupérer l’archive associé
  à l’identifiant du modèle.

## Le répertoire middleware

Ce répertoire contient la déclaration des
[middlewares](https://expressjs.com/fr/guide/writing-middleware.html)
pour la librairie Express.

- Le fichiers _workspace-upload.js_ permet d’uploader des espace de
  travail en se basant sur le module
  [multer](https://github.com/expressjs/multer) . La function storage
  de ce fichier est en charge du stockage du flux émis par le client
  dans le répertoire

## Le répertoire services

Ce répertoire contient l’ensemble des fonctions de traitement des
requêtes pour les différents objets manipulés par les services REST
déclarés dans la partie _controllers_ .

Le fichier _constants.js_ décrit l’ensemble des constantes pour
l’application (répertoires, nom de fichiers, …).

Le fichier _contours.js_ permet le filtrage d’une liste de couche
contour en se basant sur la maille et l’échelle (fonction
filterOnMailleAndEchelle).

Le fichier dataSet.js permet le filtrage d’une liste de couche data en
se basant sur la maille et l’échelle (fonction
filterOnMailleAndEchelle).

Le fichier _readArchive.js_ est en charge de la gestion des archives :

- La méthode readZipArchive permet de récupérer, pour un répertoire,
  dé récupérer les données contenues dans un fichier précis de chaque
  archive ZIP de ce répertoire. On s’en sert pour lister les
  différents éléments stockés sur le serveur (couches, fond de cartes,
  …).

- La méthode getZipPath permet de récupérer le chemin d’un fichier
  contenu dans un répertoire en fonction de son nom ou de sa position
  dans le répertoire.

<span id="_Toc116638814" class="anchor"></span>
